package iss.nus.edu.sg.pillbox;

import iss.nus.edu.sg.pillbox.services.BluetoothService;
import iss.nus.edu.sg.pillbox.services.CommonService;
import iss.nus.edu.sg.pillbox.services.KeyboardService;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;

/**
 * Main file to run the app
 * This app will create two threads. 1 for listens keyboard pressed to capture image
 * and the other one for listens phone bluetooth (configured) to send the images captured
 * 
 * @author Ha
 *
 */
public class Pillbox {

	private static final Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
	public static void main(String[] args) {
		// Disable parent logger and set the desired level.
		logger.setUseParentHandlers(false);
		logger.setLevel(Level.ALL);

		// Add our custom formatter to a console handler.
		ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(Level.WARNING);
		logger.addHandler(handler);
		// start bluetooth service
		CommonService bService = new BluetoothService();
		// start keyboard service
		CommonService kService = new KeyboardService();
		bService.start();
		kService.start();
	}
}
