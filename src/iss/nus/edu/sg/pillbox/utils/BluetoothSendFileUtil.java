package iss.nus.edu.sg.pillbox.utils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;
import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

/**
 * 
 * This util will connect to phone via bluetooth and send the image file to the phone
 * 
 * @author Ha
 *
 */
public class BluetoothSendFileUtil {
	private static String serverURL = "btgoep://"+PropertyUtil.getProperty(Constant.DEFAULT_BLUETOOTH_DEVICE_ID)+":12;authenticate=false;encrypt=false;master=false";
	private static ClientSession clientSession = null;
	
	/**
	 * 
	 * @return
	 */
	public static boolean init() {
		if (clientSession == null) {
			try {
				// init bluetooth session
				clientSession = (ClientSession) Connector.open(serverURL);
				HeaderSet hsConnectReply = clientSession.connect(null);
		        if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
		        	clientSession = null;
		        }
			} catch (IOException e) {
				System.out.println("Can not connect to devide: " + serverURL);
			}
		}
		if (clientSession != null) {
			System.out.println("Bluetooth connected.");
			return true;
		} else {
			System.out.println("Failed to connect bluetooth device.");
		}
		return false;
	}
	
	/**
	 * Send file to phone
	 * @param fileSend
	 * @return true if successful and else
	 */
	public static boolean sendFile(File fileSend) {
		if (clientSession != null) {
			try {
				// create header based on the file selected
				byte data[] = Files.readAllBytes(fileSend.toPath());
		        HeaderSet hsOperation = clientSession.createHeaderSet();
		        hsOperation.setHeader(HeaderSet.NAME, fileSend.getName());
		        hsOperation.setHeader(HeaderSet.TYPE, new MimetypesFileTypeMap().getContentType(fileSend));
		        hsOperation.setHeader(HeaderSet.LENGTH, Long.valueOf(data.length)); 
		        
		        //Create PUT Operation
		        Operation putOperation = clientSession.put(hsOperation);
		        
		        // Send file to server
		        OutputStream os = putOperation.openOutputStream();
		        os.write(data);
		        os.flush();
		        os.close();

		        // check if sent successful or rejected/failed
		        if (putOperation.getResponseCode() == ResponseCodes.OBEX_HTTP_OK) {
		        	putOperation.close();
		        	return true;
		        }
			} catch (Exception e) {
				clientSession = null;
			}
		}
		return false;
	}
}
