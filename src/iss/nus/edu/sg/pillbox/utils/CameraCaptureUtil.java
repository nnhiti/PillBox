package iss.nus.edu.sg.pillbox.utils;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.fswebcam.FsWebcamDriver;

/**
 * 
 * This class will capture image from camera and 
 * store to bluetooth folder (configured in config file)
 * 
 * @author Ha
 *
 */
public class CameraCaptureUtil {

	private static Webcam webcam = null;
	public static boolean init() {
		if (webcam == null) {
			try {
				// init webcam device
				Webcam.setDriver(new FsWebcamDriver());
				webcam = Webcam.getDefault();
			} catch(Exception e) {}
		}
		if (webcam != null) {
			System.out.println("Webcam initialized: " + webcam.getName());
			return true;
		} else {
			System.out.println("No webcam detected");
		}
		return false;
	}
	
	/**
	 * Capture image and store it
	 * @throws IOException
	 */
	public static void capture() throws IOException {
		if (webcam != null) {
			webcam.setViewSize(new Dimension(Integer.parseInt(PropertyUtil.getProperty(Constant.IMAGE_WIDTH)), Integer.parseInt(PropertyUtil.getProperty(Constant.IMAGE_HEIGHT))));
			webcam.open();
			File bluetoothFolder = new File(StringUtil.getBlueToothFolder());
			bluetoothFolder.mkdirs();
			SimpleDateFormat sdfDate = new SimpleDateFormat(PropertyUtil.getProperty(Constant.IMAGE_FILENAME_FORMAT));
			String imageType = PropertyUtil.getProperty(Constant.IMAGE_FILETYPE);
			ImageIO.write(webcam.getImage(), imageType, new File(bluetoothFolder.getPath()+File.separator+"PILLBOX-"+sdfDate.format(new Date())+"."+imageType));
			webcam.close();
		}
	}
}
