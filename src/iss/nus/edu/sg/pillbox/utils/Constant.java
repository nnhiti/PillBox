package iss.nus.edu.sg.pillbox.utils;

/**
 * 
 * Constants for config file
 * 
 * @author Ha
 *
 */
public class Constant {
	public static final String DEFAULT_BLUETOOTH_FOLDER = "BLUETOOTH_FOLDER";
	public static final String IMAGE_FILENAME_FORMAT = "IMAGE_FILENAME_FORMAT";
	public static final String IMAGE_FILETYPE = "IMAGE_FILETYPE";
	public static final String IMAGE_WIDTH = "IMAGE_WIDTH";
	public static final String IMAGE_HEIGHT = "IMAGE_HEIGHT";
	public static final String DEFAULT_BLUETOOTH_DEVICE_ID = "BLUETOOTH_DEVICE_ID";
	public static final String COMMAND = "COMMAND";
}
