package iss.nus.edu.sg.pillbox.utils;

import iss.nus.edu.sg.pillbox.Pillbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * Utility for reading configs
 * 
 * @author Ha
 *
 */
public class PropertyUtil {
	private static Properties prop = null;
	static {
		if (prop == null) {
			prop = new Properties();
			InputStream input = null;
			try {
				String path = Pillbox.class.getProtectionDomain().getCodeSource().getLocation().getPath();
				if (path.lastIndexOf(".jar") > 0) {
					path = path.substring(0, path.lastIndexOf("/"));
				}
				input = new FileInputStream(path+File.separator+"config"+File.separator+"config.properties");
				// load a properties file
				prop.load(input);
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * get property value by key
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
}
