package iss.nus.edu.sg.pillbox.utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * String Util
 * 
 * @author Ha
 *
 */
public class StringUtil {

	/**
	 * get path of the project
	 * @return
	 */
	public static String getProjectPath() {
		Path currentRelativePath = Paths.get("");
		return currentRelativePath.toAbsolutePath().toString();
	}
	
	/**
	 * get bluetooth folder path
	 * @return
	 */
	public static String getBlueToothFolder() {
		return getProjectPath()+File.separator+PropertyUtil.getProperty(Constant.DEFAULT_BLUETOOTH_FOLDER);
	}
}
