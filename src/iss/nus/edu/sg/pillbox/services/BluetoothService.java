package iss.nus.edu.sg.pillbox.services;

import iss.nus.edu.sg.pillbox.utils.BluetoothSendFileUtil;
import iss.nus.edu.sg.pillbox.utils.Constant;
import iss.nus.edu.sg.pillbox.utils.LiquidCrystalI2C;
import iss.nus.edu.sg.pillbox.utils.PropertyUtil;
import iss.nus.edu.sg.pillbox.utils.StringUtil;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 
 * This service will send images to phone via bluetooth
 * and also print the message to ioc screen
 * 
 * @author Ha
 *
 */
public class BluetoothService extends CommonService {

	private LiquidCrystalI2C lcd = null;
	
	public BluetoothService(){
		threadName = BluetoothService.class.getName();
	}
	
	@Override
	public void processing() {
		try {
			lcd = new LiquidCrystalI2C(1, 0x27, 20, 4);
			lcd.init();
			lcd.backlight(true);
			lcd.blink(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// always run except the error happen
		while(true) {
			try {
				printToLcd("Please Scan Pill.");
				File bluetoothFolder = new File(StringUtil.getBlueToothFolder());
				for (final File image : bluetoothFolder.listFiles()) {
					boolean needRestart = false;
			        if (!image.isDirectory()) {
			        	printToLcd("Sending...");
			        	if (BluetoothSendFileUtil.init()) {
			        		if (BluetoothSendFileUtil.sendFile(image)) {
			        			image.delete();
			        			printToLcd("Completed!");
			        		} else {
			        			printToLcd("Rejected!");
			        			needRestart = true;
			        		}
			        	} else {
			        		printToLcd("Send Failed!");
			        		needRestart = true;
			        	}
			        	Thread.sleep(2000);
			        	// need to restart the program if the phone reject the file
			        	// if not the phone can not receive the file as same bluetooth session
			        	if (needRestart) {
			        		runApp();
			        	}
			        }
			    }
				Thread.sleep(5000);
			} catch (Exception e) {}
		}
	}
	
	/**
	 * Print msg to screen
	 * @param msg
	 * @throws IOException
	 */
	private void printToLcd(String msg) throws IOException {
		lcd.clear();
    	lcd.print(msg);
	}
	
	private void runApp() {
		try {
		    // Execute command
		    String command = PropertyUtil.getProperty(Constant.COMMAND);
		    Process child = Runtime.getRuntime().exec(command);

		    // Get output stream to write from it
		    OutputStream out = child.getOutputStream();
		    out.close();
		    System.exit(1);
		} catch (IOException e) {
		}
	}
}
