package iss.nus.edu.sg.pillbox.services;

public abstract class CommonService implements Runnable {
	protected Thread thread;
	protected String threadName;

	@Override
	public void run() {
		System.out.println(threadName + " is running.");
		processing();
	}

	// need to override this method for processing
	public abstract void processing();
	
	/**
	 * start the thread
	 */
	public void start() {
		System.out.println("Starting " + threadName);
		if (thread == null) {
			thread = new Thread(this, threadName);
			thread.start();
		}
	}
}
