package iss.nus.edu.sg.pillbox.services;

import iss.nus.edu.sg.pillbox.utils.CameraCaptureUtil;

import java.io.IOException;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/**
 * 
 * This service will listen keys pressed and then capture image and store it
 * only space bar and enter key accepted
 * 
 * @author Ha
 *
 */
public class KeyboardService extends CommonService implements NativeKeyListener {

	public KeyboardService(){
		threadName = KeyboardService.class.getName();
	}
	
	@Override
	public void processing() {
		try {
			// init and ready for keyboard hook
			GlobalScreen.registerNativeHook();
			GlobalScreen.addNativeKeyListener(this);
		} catch (NativeHookException e) {}
	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent e) {
		// only space bar and enter are accepted
		if (e.getKeyCode() == 28 || e.getKeyCode() == 57) {
			if (CameraCaptureUtil.init()) {
				try {
					CameraCaptureUtil.capture();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void nativeKeyPressed(NativeKeyEvent arg0) {}

	@Override
	public void nativeKeyTyped(NativeKeyEvent arg0) {}
}
